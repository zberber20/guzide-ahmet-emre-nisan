import { useEffect, useState } from 'react';
import './App.css';
import rings from './images/rings.png'
import left from './images/swirl-left.png'
import right from './images/swirl-right.png'

function App() {

  const address = "https://www.google.com/maps/place/Teomanpa%C5%9Fa,+Necip+Faz%C4%B1l+K%C4%B1sak%C3%BCrek+Cd+No:98,+07260+Kepez%2FAntalya/@36.9176021,30.7235639,18.71z/data=!4m6!3m5!1s0x14c38570888cac83:0x51afbca8bb0e8ef4!8m2!3d36.9177051!4d30.7242952!16s%2Fg%2F11c22yl5_s?entry=ttu"


  function calculateTimeLeft() {
    const difference = +new Date('November 25, 2023 19:00:00') - +new Date();
    let timeLeft = {};
  
    if (difference > 0) {
      timeLeft = {
        days: Math.floor(difference / (1000 * 60 * 60 * 24)) < 10 ? `0${Math.floor(difference / (1000 * 60 * 60 * 24))}` : Math.floor(difference / (1000 * 60 * 60 * 24)),
        hours: Math.floor((difference / (1000 * 60 * 60)) % 24) < 10 ? `0${Math.floor((difference / (1000 * 60 * 60)) % 24)}` : Math.floor((difference / (1000 * 60 * 60)) % 24),
        minutes: Math.floor((difference / 1000 / 60) % 60) < 10 ? `0${Math.floor((difference / 1000 / 60) % 60)}` : Math.floor((difference / 1000 / 60) % 60),
        seconds: Math.floor((difference / 1000) % 60) < 10 ? `0${Math.floor((difference / 1000) % 60)}` : Math.floor((difference / 1000) % 60)
      };
    }
  
    return timeLeft;
  }

  const [timeLeft, setTimeLeft] = useState(calculateTimeLeft());

  useEffect(() => {
    const id = setTimeout(() => {
      setTimeLeft(calculateTimeLeft());
    }, 1000);

    return () => {
      clearTimeout(id);
    };
  });

  return (
    <div className="main-wrapper">
    <div className="main">
      <div className='photo-main'>
        <div class="blur"></div>
      </div>

      <div className='white center pt-4'>nişanlanıyoruz</div>
      <div className='rings-logo'>
        <img src={rings} alt="rings-logo" />
      </div>
      <div className='white center pb-4'>sizleri de bekleriz...</div>

      <div className='names white clicker'>
        Güzide{' '}<span className='gold clicker'>&</span>{' '}A.Emre
      </div>

      <div className='center date-wrapper pt-4'>
        <img src={left} alt="" />
        <div style={{marginLeft: '10px', marginRight: '10px'}}>
          <span className='white'>25</span>
          <span className='gold'>.</span>
          <span className='white'>11</span>
          <span className='gold'>.</span>
          <span className='white'>2023</span>
        </div>
        <img src={right} alt="" />
      </div>
      <div className='center date-wrapper white' style={{fontSize: "16px", marginBottom: '20px'}}>
      <span className='white'>19</span>
          <span className='gold'>:</span>
          <span className='white'>00</span>
      </div>


      <div className='countdown-wrapper'>
        <div className='countdown day'>{timeLeft.days}</div><span className='white'>:</span>
        <div className='countdown hour'>{timeLeft.hours}</div><span className='white'>:</span>
        <div className='countdown mins'>{timeLeft.minutes}</div><span className='white'>:</span>
        <div className='countdown secs'>{timeLeft.seconds}</div>
      </div>

      <div className='center white pt-4'>
        <a href={address} target='_blank' rel="noreferrer" style={{textDecoration: 'none'}} className='white'>nişan yeri</a>
      </div>

      

    </div>
    </div>
  );
}

export default App;
